package main.java.cefalo.school.TemplateMethod;

/**
 * Created by sheralam on 9/21/16.
 */
public class MergeSort  extends SortingTemplate{

    private Object[] tempMergArr;

    protected  void initialization(Object[] array) {
        inputArray = array;
        this.tempMergArr = new Object[array.length];
        System.out.println("Merge Sort Initialized");
    }

    @Override
    protected void DivideAndConquer(int lowerIndex, int higherIndex) {
        if (lowerIndex < higherIndex) {
            int middle = lowerIndex + (higherIndex - lowerIndex) / 2;

            DivideAndConquer(lowerIndex, middle);

            DivideAndConquer(middle + 1, higherIndex);
            
            mergeParts(lowerIndex, middle, higherIndex);
        }
    }

    @Override
    protected void print() {
        System.out.println("Merge Sort Completed. Printing the array");
        super.print();
    }

    private void mergeParts(int lowerIndex, int middle, int higherIndex) {

        for (int i = lowerIndex; i <= higherIndex; i++) {
            tempMergArr[i] = inputArray[i];
        }
        int i = lowerIndex;
        int j = middle + 1;
        int k = lowerIndex;
        while (i <= middle && j <= higherIndex) {
            if (compare(tempMergArr[i] , tempMergArr[j]) <= 0) {
                inputArray[k] = tempMergArr[i];
                i++;
            } else {
                inputArray[k] = tempMergArr[j];
                j++;
            }
            k++;
        }
        while (i <= middle) {
            inputArray[k] = tempMergArr[i];
            k++;
            i++;
        }

    }
}

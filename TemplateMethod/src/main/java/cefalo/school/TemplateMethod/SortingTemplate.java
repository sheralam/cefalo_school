package main.java.cefalo.school.TemplateMethod;

/**
 * Created by sheralam on 9/21/16.
 */
public abstract class SortingTemplate {

    protected Object[] inputArray;


    public final void sort(Object[] array, int lowerIndex, int higherIndex) {
        initialization(array);
        DivideAndConquer(lowerIndex, higherIndex);
        print();
    }

    abstract protected void initialization(Object[] array);

    abstract protected void DivideAndConquer(int lowerIndex, int higherIndex);


    private int compare(Integer a, Integer b) {
        return a.compareTo(b);
    }

    private int compare(String str1, String str2) {
        return str1.compareTo(str2);
    }

    protected final int compare(Object obj1, Object obj2) {
        if ((obj1 instanceof String) && (obj2 instanceof String)) {
            return compare((String) obj1, (String) obj2);
        } else {
            return compare((Integer) obj1, (Integer) obj2);
        }
    }

    protected void print() {
        if (inputArray != null) {
            for (Object obj : inputArray) {
                System.out.println(obj);
            }
        }
        System.out.println("\nDone Sorting !!!\n\n");


    }
}

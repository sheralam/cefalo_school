package main.java.cefalo.school.TemplateMethod;

import java.util.Scanner;

/**
 * Created by sheralam on 9/21/16.
 */
public class Main {
    public static void main(String[] arg){
        System.out.println("cefalo.school.designpattern\n\n");
         Object[] inputArray = {45,23,11,89,77,98,4,28,65,43};
         Object[] inputStrArray = {"cba","abc","bcc","xyz","afsd","vaaf","ryrt"};


        while(true){
            System.out.print("Integer Array :  ");
            for (Object obj : inputArray) {
                System.out.print(obj + ",");
            }
            System.out.print("\nString Array : ");
            for (Object obj : inputStrArray) {
                System.out.print(obj+",");
            }
            Scanner Cin = new Scanner(System.in);
            System.out.print("\nEnter 1 to sort Integer array , Enter 2 for String array :");
            int arrayChoice = Cin.nextInt();
            System.out.print("\nEnter 1 to sort Integer array , Enter 2 for String array :");
            int algoChoice = Cin.nextInt();

            if(algoChoice == 1){
                if(arrayChoice == 1){
                    new MergeSort().sort(inputArray,0,inputArray.length-1);
                }else if(arrayChoice == 2){
                    new MergeSort().sort(inputStrArray,0,inputStrArray.length-1);
                }else {
                    System.err.print("\n#### Invalid Choice of Array");
                }

            }else if(algoChoice == 2){
                if(arrayChoice == 1){
                    new QuickSort().sort(inputArray,0,inputArray.length-1);
                }else if(arrayChoice == 2){
                    new QuickSort().sort(inputStrArray,0,inputStrArray.length-1);
                }else {
                    System.err.print("\n#### Invalid Choice of Array");
                }
            }else {
                System.err.print("\n#### Invalid Choice of Algorithm");
            }




        }


    }
}

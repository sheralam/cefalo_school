package main.java.cefalo.school.TemplateMethod;

/**
 * Created by sheralam on 9/21/16.
 */
public class QuickSort extends SortingTemplate{

    @Override
    protected  void initialization(Object[] array) {
        inputArray = array;
        System.out.println("Quick Sort Initialized");
    }

    @Override
    protected void DivideAndConquer(int lowerIndex, int higherIndex) {
        System.out.println("Quick Sort Started ");
        int i = lowerIndex;
        int j = higherIndex;

        Object pivot = inputArray[lowerIndex+(higherIndex-lowerIndex)/2];
        while (i <= j) {
            while (compare(inputArray[i] , pivot) < 0) {
                i++;
            }
            while (compare(inputArray[j] , pivot) > 0) {
                j--;
            }
            if (i <= j) {
                exchangeNumbers(i, j);
                i++;
                j--;
            }
        }
        if (lowerIndex < j)
            DivideAndConquer(lowerIndex, j);
        if (i < higherIndex)
            DivideAndConquer(i, higherIndex);
    }

    private void exchangeNumbers(int i, int j) {
        Object temp = inputArray[i];
        inputArray[i] = inputArray[j];
        inputArray[j] = temp;
    }

    @Override
    protected void print() {
        System.out.println("Quick Sort Completed. Printing the array");
        super.print();
    }
}

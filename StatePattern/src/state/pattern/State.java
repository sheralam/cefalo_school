package state.pattern;

/**
 * Created by sher on 12/3/2016.
 */
public interface State {
    public boolean performAction(Vehicle vehicle);
    public boolean validate(State state);
    public int getFuelConsumption();
}

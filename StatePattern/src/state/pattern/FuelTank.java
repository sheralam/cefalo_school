package state.pattern;

/**
 * Created by sher on 12/3/2016.
 */
public class FuelTank {
    int Fuel =100;

    public FuelTank(){}

    public FuelTank(int fuel){
        this.Fuel = fuel;
    }
    public int getFuel() {
        return Fuel;
    }

    public void setFuel(int fuel) {
        Fuel = fuel;
    }

    public boolean update(int fuel) {
        if(Fuel-fuel > 5){
            Fuel -= fuel;
            return true;
        }
        return false;

    }

}

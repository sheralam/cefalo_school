package state.pattern;

/**
 * Created by sher on 12/3/2016.
 */
public class Vehicle {
    private State currentState;
    private FuelTank fuelTank;
    public Vehicle(State state){
        fuelTank = new FuelTank(20);
        currentState = state;
    }



    public boolean setState(State state){
        if(updateFueltank(state.getFuelConsumption())){
            this.currentState = state;
            return true;
        }
        System.out.print("No More Fuel to change state");
        return false;

    }

    public State getState(){
        return currentState;
    }


    boolean updateFueltank(int expense){
        return fuelTank.update(expense);
    }

    void fillupFueltank(int fuel){
         fuelTank.setFuel(fuel);
    }
}

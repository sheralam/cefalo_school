package state.pattern;

import java.util.Scanner;

/**
 * Created by sher on 12/3/2016.
 */
public class Main {
    public static void main(String[] arg){

        print("#######################################################");
        print("###############    STATE PATTERN    ###################");
        print("### Your Car is ready, Please fasten your seat belt ###");
        print("#######################################################");

        Scanner in = new Scanner(System.in);
        StartEngineState startEngineState = new StartEngineState(StopState.class.toString());
        DrivingState drivingState = new DrivingState(StartEngineState.class.toString());
        StopState stopState = new StopState(DrivingState.class.toString());
        int num;
        Vehicle car = new Vehicle(stopState);
        while(true) {
            print("\n\nEnter 1 to start, 2 to drive, 3 to park the car, 9 to Refill Fuel");
            print("Current State : "+car.getState().toString()+"\n\n-->");
            num = in.nextInt();
            switch (num){
                case 3:
                    if(stopState.validate(car.getState())){
                        stopState.performAction(car);
                    }else {
                        print("### Can not switch to "+stopState.toString());
                    }
                    break;

                case 1:
                    if(startEngineState.validate(car.getState())){
                        startEngineState.performAction(car);
                    }else {
                        print("###  Can not switch to "+startEngineState.toString());
                    }

                    break;
                case 2:
                    if(drivingState.validate(car.getState())){
                        drivingState.performAction(car);
                    }else {
                        print("###  Can not switch to "+drivingState.toString());
                    }
                    break;
                case 9:
                    car.fillupFueltank(20);
                    print("### Refilled ####");
                    break;
                default:


            }

        }

    }

    public static void print(String s) {
        System.out.println(s);
    }
}

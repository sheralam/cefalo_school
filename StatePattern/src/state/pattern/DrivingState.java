package state.pattern;

/**
 * Created by sher on 12/3/2016.
 */
public class DrivingState  implements State {
    int fuelConsumption = 5;
    String preRequiredState;

    public DrivingState(String preReqState){
        this.preRequiredState = preReqState;
    }


    @Override
    public int getFuelConsumption() {
        return fuelConsumption;
    }



    @Override
    public boolean performAction(Vehicle vehicle) {
        if(validate(vehicle.getState())) {
            System.out.println(this.toString());
            vehicle.setState(this);
            return true;
        }
        return false;
    }

    @Override
    public boolean validate(State state){
        return state.getClass().toString().equalsIgnoreCase(preRequiredState);
    }

    public String toString(){
        return "Driving now...";
    }
}

package command.pattern;

import command.pattern.person.Person;

import java.util.List;

/**
 * Created by sher on 12/10/2016.
 */
public class Create implements CRUDCommand{

    Person person;
    private int index;
    public Create(Person p){
        person = p;
    }

    @Override
    public void executeCommand(List list) {
        System.out.println("Creating Person " + person.getName());
        index = list.size();
        list.add(person);
    }

    @Override
    public void undoCommand(List list) {
        System.out.println(this.getClass()+" UNDO");
        list.remove(index);
    }

    @Override
    public void redoCommand(List list) {
        System.out.println(this.getClass()+" REDO");
        executeCommand(list);
    }
}

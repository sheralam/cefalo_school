package command.pattern;

import command.pattern.person.Person;

import java.io.InvalidObjectException;
import java.util.List;

/**
 * Created by sher on 12/10/2016.
 */
public class Update implements CRUDCommand {

    Person person;
    int index;
    String storedName;
    public Update(int i,String name){
        index = i;
        storedName = name;
    }


    @Override
    public void executeCommand(List list) {
        System.out.println("---------- Executing Update List Command");
        try {
            Person tmp = ((Person)list.get(index));
            String name = tmp.getName();
            tmp.setName(storedName);
            storedName = name;
        }catch (IndexOutOfBoundsException e){
            System.out.println(this.getClass().getSimpleName()+" ERROR :   Invalid Index");

        }

    }

    @Override
    public void undoCommand(List list) {
        System.out.println(this.getClass()+" UNDO");
        executeCommand(list);
    }

    @Override
    public void redoCommand(List list) {
        System.out.println(this.getClass()+" REDO");
        executeCommand(list);
    }
}

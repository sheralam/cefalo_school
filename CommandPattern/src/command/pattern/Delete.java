package command.pattern;

import command.pattern.person.Person;

import java.util.List;

/**
 * Created by sher on 12/10/2016.
 */
public class Delete implements CRUDCommand {
    Person person;
    public Delete(){
    }

    @Override
    public void executeCommand(List list) {

        person = (Person)list.get(list.size()-1);
        list.remove(list.size()-1);

        System.out.println("----------  Deleting person "+ person.getName() );
    }

    @Override
    public void undoCommand(List list) {
        System.out.println(this.getClass()+" UNDO");
        list.add(person);
    }

    @Override
    public void redoCommand(List list) {
        System.out.println(this.getClass()+" REDO");
        executeCommand(list);
    }
}

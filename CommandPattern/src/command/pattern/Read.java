package command.pattern;

import command.pattern.person.Person;

import java.util.List;

/**
 * Created by sher on 12/10/2016.
 */
public class Read implements CRUDCommand {

    public Read(){
    }


    @Override
    public void executeCommand(List<Person> list) {
        System.out.println("############## Executing Read List Command");
        list.forEach(System.out::println);
    }

    @Override
    public void undoCommand(List list) {
        System.out.println(this.getClass()+" UNDO");

    }

    @Override
    public void redoCommand(List list) {
        System.out.println(this.getClass()+" REDO");

    }
}

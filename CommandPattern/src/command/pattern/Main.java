package command.pattern;

import command.pattern.person.Female;
import command.pattern.person.Male;
import command.pattern.person.Person;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sher on 12/10/2016.
 */
public class Main {
    public static void main(String[] ar){
        List personlist = new ArrayList<Person>();

        Create create1 = new Create(new Male("Sajib"));
        create1.executeCommand(personlist);
        Create create2 = new Create(new Male("Rajib"));
        create2.executeCommand(personlist);
        Create create3 = new Create(new Female("Jennifer"));
        create3.executeCommand(personlist);
        Create create4 = new Create(new Female("Madonna"));
        create4.executeCommand(personlist);
        Delete delete1 = new Delete();
        delete1.executeCommand(personlist);
        Delete delete2 = new Delete();
        delete2.executeCommand(personlist);
        Read read = new Read();
        read.executeCommand(personlist);

        delete2.undoCommand(personlist);
        create2.undoCommand(personlist);

        read.executeCommand(personlist);
        Update update1 = new Update(0,"Mr Sajib");
        update1.executeCommand(personlist);
        Update update3 = new Update(1,"Mrs Asha");
        update3.executeCommand(personlist);
        read.executeCommand(personlist);





    }
}

package command.pattern.person;

/**
 * Created by sher on 12/10/2016.
 */
public class Female extends Person{

    public Female(String name){
        super(Gender.FEMALE);
        setName(name);
    }

    @Override
    public String toString() {
        return "Her Name is "+Name;
    }
}

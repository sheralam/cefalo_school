package command.pattern.person;

/**
 * Created by sher on 12/10/2016.
 */
public class Male extends Person{

    public Male(String name){
        super(Gender.MALE);
        setName(name);
    }

    @Override
    public String toString() {
        return "His Name is "+Name;
    }
}

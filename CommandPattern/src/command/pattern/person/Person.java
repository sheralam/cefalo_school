package command.pattern.person;

/**
 * Created by sher on 12/10/2016.
 */
public abstract class Person {
    protected final Gender gender;
    protected String Name;
    public Person(Gender gender){
        this.gender = gender;
    }

    public  Gender getGender(){
        return gender;
    }

    public   String getName(){
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}



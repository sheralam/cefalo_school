package command.pattern;

import command.pattern.person.Person;

import java.util.List;

/**
 * Created by sher on 12/10/2016.
 */
public interface CRUDCommand {
    public void executeCommand(List<Person> list);
    public void undoCommand(List<Person> list);
    public void redoCommand(List<Person> list);

}

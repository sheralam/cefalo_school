package builder.pattern.product;

/**
 * Created by sher on 12/3/2016.
 */
public enum BuildingType {
    BRICK_HOUSE,GLASS_HOUSE,WOOD_HOUSE;
}

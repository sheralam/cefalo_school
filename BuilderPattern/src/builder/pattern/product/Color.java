package builder.pattern.product;

/**
 * Created by sher on 12/3/2016.
 */
public enum Color {
    WHITE,BLUE,PINK,RED,GREEN,BLACK,GREY,BROWN;
}

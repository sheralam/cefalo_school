package builder.pattern.product;

/**
 * Created by sher on 12/3/2016.
 */
public class Building {
    BuildingType buildingType;
    int numOfFloors;
    int numOfRooms;
    int numOfDoors;
    int numOfWindows;
    Color buildingColor;
    Color floorColor;

    public BuildingType getBuildingType() {
        return buildingType;
    }

    public void setBuildingType(BuildingType buildingType) {
        this.buildingType = buildingType;
    }

    public int getNumOfFloors() {
        return numOfFloors;
    }

    public void setNumOfFloors(int numOfFloors) {
        this.numOfFloors = numOfFloors;
    }

    public int getNumOfRooms() {
        return numOfRooms;
    }

    public void setNumOfRooms(int numOfRooms) {
        this.numOfRooms = numOfRooms;
    }

    public int getNumOfDoors() {
        return numOfDoors;
    }

    public void setNumOfDoors(int numOfDoors) {
        this.numOfDoors = numOfDoors;
    }

    public int getNumOfWindows() {
        return numOfWindows;
    }

    public void setNumOfWindows(int numOfWindows) {
        this.numOfWindows = numOfWindows;
    }

    public Color getBuildingColor() {
        return buildingColor;
    }

    public void setBuildingColor(Color buildingColor) {
        this.buildingColor = buildingColor;
    }

    public Color getFloorColor() {
        return floorColor;
    }

    public void setFloorColor(Color floorColor) {
        this.floorColor = floorColor;
    }

    @Override
    public String toString(){
        return getBuildingType()+"::  with Floors: "+getNumOfFloors()+", Rooms: "+getNumOfRooms()+", Doors: "+getNumOfDoors()+", Windows: "+getNumOfWindows();
    }

}

package builder.pattern;

import builder.pattern.builders.*;
import builder.pattern.directors.Director;
import builder.pattern.directors.MultiStoryDirector;
import builder.pattern.directors.SingleStoryDirector;
import builder.pattern.product.BuildingType;
import builder.pattern.product.Color;

/**
 * Created by sher on 12/3/2016.
 */
public class Main {
    public static void main(String[] arg){

        print("########################################################");
        print("###############    BUILDER PATTERN    ###################");
        print("### Your Directors and Builders are ready to work :) ###");
        print("########################################################\n\n\n");


        Requirements reqWood= new Requirements(BuildingType.WOOD_HOUSE,5,2,5, Color.BROWN,Color.BLACK);
        Requirements reqBrick= new Requirements(BuildingType.BRICK_HOUSE,15,2,20, Color.GREEN,Color.BROWN);
        Requirements reqGlass= new Requirements(BuildingType.GLASS_HOUSE,4,2,4, Color.PINK,Color.BLUE);

        Builder woodBuilder = new WoodHouseBuilder();
        Builder brickBuilder = new BrickHouseBuilder();
        Builder glassBuilder = new GlassHouseBuilder();


        print("Building ... Single Story Wood House");
        Director directorSingleWood = new SingleStoryDirector(woodBuilder);
        directorSingleWood.constructBuilding(reqWood);
        print("\nConstruction Complete :: "+directorSingleWood.getBuilding()+"\n\n");

        print("Building ... Multi Story Wood House");
        Director directorMultyWood = new MultiStoryDirector(woodBuilder,3);
        directorMultyWood.constructBuilding(reqWood);
        print("\nConstruction Complete :: "+directorMultyWood.getBuilding()+"\n\n");

        print("Building ... Single Story Brick House");
        Director directorSingleBrick = new SingleStoryDirector(brickBuilder);
        directorSingleBrick.constructBuilding(reqBrick);
        print("\nConstruction Complete :: "+directorSingleBrick.getBuilding()+"\n\n");

        print("Building ... Multi Story Brick House");
        Director directorMultyBrick = new MultiStoryDirector(brickBuilder,9);
        directorMultyBrick.constructBuilding(reqBrick);
        print("\nConstruction Complete :: "+directorMultyBrick.getBuilding()+"\n\n");

        print("Building ... Single Story Glass House");
        Director directorSingleGlass = new SingleStoryDirector(glassBuilder);
        directorSingleGlass.constructBuilding(reqGlass);
        print("\nConstruction Complete :: "+directorSingleGlass.getBuilding()+"\n\n");

        print("Building ... Multi Story Glass House");
        Director directorMultyGlass = new MultiStoryDirector(glassBuilder,2);
        directorMultyGlass.constructBuilding(reqGlass);
        print("\nConstruction Complete :: "+directorMultyGlass.getBuilding()+"\n\n");


    }

    public static void print(String s) {
        System.out.println(s);



    }

}

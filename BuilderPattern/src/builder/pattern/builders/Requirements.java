package builder.pattern.builders;

import builder.pattern.product.BuildingType;
import builder.pattern.product.Color;

/**
 * Created by sher on 12/3/2016.
 */
public class Requirements {
    BuildingType buildingType;
    int numOfRooms;
    int numOfDoors;
    int numOfWindows;
    Color buildingColor;
    Color floorColor;

    public Requirements(BuildingType buildingType, int numOfRooms, int numOfDoors, int numOfWindows, Color buildingColor, Color floorColor) {
        this.buildingType = buildingType;
        this.numOfRooms = numOfRooms;
        this.numOfDoors = numOfDoors;
        this.numOfWindows = numOfWindows;
        this.buildingColor = buildingColor;
        this.floorColor = floorColor;
    }

    public BuildingType getBuildingType() {
        return buildingType;
    }

    public int getNumOfRooms() {
        return numOfRooms;
    }

    public int getNumOfDoors() {
        return numOfDoors;
    }

    public int getNumOfWindows() {
        return numOfWindows;
    }

    public Color getBuildingColor() {
        return buildingColor;
    }

    public Color getFloorColor() {
        return floorColor;
    }
}

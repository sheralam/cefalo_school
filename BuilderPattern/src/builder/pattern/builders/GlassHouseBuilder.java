package builder.pattern.builders;

import builder.pattern.product.Building;
import builder.pattern.product.BuildingType;
import builder.pattern.product.Color;

/**
 * Created by sher on 12/3/2016.
 */
public class GlassHouseBuilder implements Builder{

    Building building;

    @Override
    public Building getBuilding() {
        return building;
    }


    public GlassHouseBuilder(){
        building = new Building();
        build();
    }

    @Override
    public void build() {
        building.setBuildingType(BuildingType.GLASS_HOUSE);
    }


    @Override
    public void buildFloors(int num) {
        System.out.print("--->Building "+num+" Floor(s)");
        building.setNumOfFloors(num);
    }

    @Override
    public void buildRooms(int num) {
        System.out.print("--->Building "+num+" Room(s)");
        building.setNumOfRooms(num);
    }

    @Override
    public void buildDoors(int num) {
        System.out.print("--->Building "+num+" Door(s)");
        building.setNumOfDoors(num);

    }

    @Override
    public void buildWindows(int num) {
        System.out.print("--->Building "+num+" Windows(s)");
        building.setNumOfWindows(num);

    }

    @Override
    public void paintBuilding(Color color) {
        System.out.print("--->Painting Builiding "+color);
        building.setBuildingColor(color);
    }

    @Override
    public void painFloor(Color color) {
        System.out.print("--->Painting Floors "+color);
        building.setFloorColor(color);

    }
}

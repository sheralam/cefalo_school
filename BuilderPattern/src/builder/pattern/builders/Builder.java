package builder.pattern.builders;

import builder.pattern.product.Building;
import builder.pattern.product.Color;

/**
 * Created by sher on 12/3/2016.
 */
public interface Builder {
    void build();
    void buildFloors(int num);
    void buildRooms(int num);
    void buildDoors(int num);
    void buildWindows(int num);
    void paintBuilding(Color color);
    void painFloor(Color color);
    Building getBuilding();
}

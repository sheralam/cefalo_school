package builder.pattern.directors;

import builder.pattern.builders.Builder;
import builder.pattern.builders.Requirements;

/**
 * Created by sher on 12/3/2016.
 */
public class MultiStoryDirector extends Director{

    public MultiStoryDirector(Builder builder, int numOfFloor){
        super(builder,numOfFloor);
    }

    @Override
    public void constructBuilding(Requirements requirements) {
        builder.buildRooms(requirements.getNumOfRooms());
        builder.buildDoors(requirements.getNumOfDoors());
        builder.buildWindows(requirements.getNumOfWindows());
        builder.paintBuilding(requirements.getBuildingColor());
        builder.painFloor(requirements.getBuildingColor());

    }
}

package builder.pattern.directors;

import builder.pattern.builders.Builder;
import builder.pattern.builders.Requirements;
import builder.pattern.product.Building;

/**
 * Created by sher on 12/3/2016.
 */
public abstract class Director {

    final protected Builder builder;

    public Director(Builder builder,int numberOfFloor){
        this.builder = builder;
        this.builder.buildFloors(numberOfFloor);
    }

    public abstract void constructBuilding(Requirements requirements);

    public final Building getBuilding(){
        return this.builder.getBuilding();
    }
}

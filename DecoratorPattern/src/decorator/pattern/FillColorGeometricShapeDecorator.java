package decorator.pattern;

/**
 * Created by sher on 12/10/2016.
 */
public class FillColorGeometricShapeDecorator extends GeometricShapesDecorator {

    private String fillColor;
    public FillColorGeometricShapeDecorator(GeometricShapes decoratedShape,String color) throws InvalidGeomatricShape {
        super(decoratedShape);
        fillColor = color;
    }

    @Override
    public void drawShape(){
            decoratedShape.drawShape();
            System.err.println("Setting fill color for "+decoratedShape.getClass().getSimpleName()+" with "+ fillColor+"\n\n");
    }
}

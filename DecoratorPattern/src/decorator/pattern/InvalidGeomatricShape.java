package decorator.pattern;

import java.io.InvalidObjectException;

/**
 * Created by sher on 12/10/2016.
 */
public class InvalidGeomatricShape extends InvalidObjectException {
    /**
     * Constructs an <code>InvalidObjectException</code>.
     *
     * @param reason Detailed message explaining the reason for the failure.
     * @see ObjectInputValidation
     */
    public InvalidGeomatricShape(String reason) {
        super("### "+reason);
    }
}

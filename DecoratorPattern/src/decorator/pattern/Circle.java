package decorator.pattern;

/**
 * Created by sher on 12/10/2016.
 */
public class Circle implements GeometricShapes {

    @Override
    public void drawShape() {
        System.out.println("Drawing "+this.getClass().getSimpleName());
    }
}

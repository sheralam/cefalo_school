package decorator.pattern;

/**
 * Created by sher on 12/10/2016.
 */
public abstract class GeometricShapesDecorator  implements GeometricShapes {
    protected GeometricShapes decoratedShape;

    public GeometricShapesDecorator(GeometricShapes decoratedShape) throws InvalidGeomatricShape {
        if(! validate(decoratedShape)){
            throw new InvalidGeomatricShape("Invalid Geometric Shape 'Line', Can not Fill with colorany ");
        }else{
            this.decoratedShape = decoratedShape;
        }
    }

    @Override
    public void drawShape(){
        decoratedShape.drawShape();
    }
    final boolean validate(GeometricShapes shapes){
        return !(decoratedShape instanceof Line);
    }

}

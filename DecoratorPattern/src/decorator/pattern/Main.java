package decorator.pattern;

/**
 * Created by sher on 12/10/2016.
 */
public class Main {
    public static void main(String[] a) throws InterruptedException {
        try {
            FillColorGeometricShapeDecorator circleShapeDecorator = new FillColorGeometricShapeDecorator(new Circle(),"RED");
            circleShapeDecorator.drawShape();
            Thread.sleep(200);
            FillColorGeometricShapeDecorator rectangleShapeDecorator = new FillColorGeometricShapeDecorator(new Rectangle(),"GREEN");
            rectangleShapeDecorator.drawShape();
            Thread.sleep(200);
            FillColorGeometricShapeDecorator lineShapeDecorator = new FillColorGeometricShapeDecorator(new Line(),"BLUE");
            lineShapeDecorator.drawShape();

        } catch (InvalidGeomatricShape invalidGeomatricShape) {
            System.err.print(invalidGeomatricShape.getLocalizedMessage());
        }


    }
}
